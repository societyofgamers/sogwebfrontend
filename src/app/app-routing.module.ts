import { CommunityComponent } from './community/community.component';
import { CounterstrikeComponent } from './counterstrike/counterstrike.component';
import { DiscordComponent } from './discord/discord.component';
import { MinecraftComponent } from './minecraft/minecraft.component';
import { HomepageComponent } from './homepage/homepage.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'minecraft', component: MinecraftComponent },
  { path: 'discord', component: DiscordComponent },
  { path: 'community', component: CommunityComponent },
  { path: 'csgo', component: CounterstrikeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
