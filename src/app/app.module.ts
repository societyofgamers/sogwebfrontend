import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationMenuComponent } from './navigation-menu/navigation-menu.component';
import { HomepageComponent } from './homepage/homepage.component';
import { MinecraftComponent } from './minecraft/minecraft.component';
import { CounterstrikeComponent } from './counterstrike/counterstrike.component';
import { DiscordComponent } from './discord/discord.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { DiscordButtonComponent } from './discord-button/discord-button.component';
import { ProfileMenuComponent } from './profile-menu/profile-menu.component';
import { BackgroundComponent } from './background/background.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CommunityComponent } from './community/community.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    MinecraftComponent,
    CounterstrikeComponent,
    DiscordComponent,
    ProfileComponent,
    SettingsComponent,
    DiscordButtonComponent,
    ProfileMenuComponent,
    BackgroundComponent,
    CommunityComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    NavigationMenuComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
