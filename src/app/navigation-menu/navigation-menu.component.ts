import { Component, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faHouse,
  faCube,
  faPersonRifle,
  faUsers,
} from '@fortawesome/free-solid-svg-icons';
import { faDiscord } from '@fortawesome/free-brands-svg-icons';
import { Router, RouterModule, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss'],
  imports: [BrowserModule, RouterModule, FontAwesomeModule],
  standalone: true,
})
export class NavigationMenuComponent implements OnInit {
  faHouse = faHouse;
  faCube = faCube;
  faDiscord = faDiscord;
  faPersonRifle = faPersonRifle;
  faUsers = faUsers;

  static AvailableStates = {
    HOME: 'home',
    CSGO: 'csgo',
    MINECRAFT: 'minecraft',
    DISCORD: 'discord',
    COMMUNITY: 'community',
  };

  ngOnInit(): void {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        switch (e.url.toLowerCase().substring(1, e.url.length)) {
          case NavigationMenuComponent.AvailableStates.CSGO:
            this.activeState = NavigationMenuComponent.AvailableStates.CSGO;
            break;
          case NavigationMenuComponent.AvailableStates.MINECRAFT:
            this.activeState =NavigationMenuComponent.AvailableStates.MINECRAFT;
            break;
          case NavigationMenuComponent.AvailableStates.COMMUNITY:
            this.activeState = NavigationMenuComponent.AvailableStates.COMMUNITY;
            break;
          case NavigationMenuComponent.AvailableStates.DISCORD:
            this.activeState = NavigationMenuComponent.AvailableStates.DISCORD;
            break;
          default:
            this.activeState = NavigationMenuComponent.AvailableStates.HOME;
            break;
        }
      }
    });
  }

  constructor(private router: Router) { }

  public activeState: string = 'home';
}
