import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CounterstrikeComponent } from './counterstrike.component';

describe('CounterstrikeComponent', () => {
  let component: CounterstrikeComponent;
  let fixture: ComponentFixture<CounterstrikeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CounterstrikeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CounterstrikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
